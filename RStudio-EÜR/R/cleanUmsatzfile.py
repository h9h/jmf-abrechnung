import re

def generateCleanFile(filename):
  """Erzeugt aus dem seltsamen Volksbank Format eine saubere CSV-Datei"""
  
  # Lese Datei
  inFile = open(filename, "r")
  text = inFile.readlines()
  inFile.close()
  
  result = ""
  # entferne Zeilen bis "Buchungstag"
  keep = False
  
  saldoAnfang = ''
  saldoEnde = ''
  for line in text:
    if 'Anfangssaldo' in line:
      saldoAnfang = line
      continue
    if 'Endsaldo' in line:
      saldoEnde = line
      continue
      
    if (line.startswith("\"Buchungstag\"")): 
      # Markiert den eigentlichen Header. Dieser Zeile fehlt ein Header für Soll/Haben
      line = line.replace("\" \"\r", "\"SH\"\r") 
      keep = True
    if (keep): 
      # Ersetze Umlaute und € und Zeilenenden
      line = line.replace('\xe4', 'ae')
      line = line.replace('\xf6', 'oe')
      line = line.replace('\xfc', 'ue')
      line = line.replace('\xc4', 'AE')
      line = line.replace('\xd6', 'OE')
      line = line.replace('\xdc', 'UE')
      line = line.replace('\xdf', 'ss')
      line = line.replace('\u20ac', "EUR")
      line = line.replace('\n', " ")
      result = result + line
    
  # schreibe neue Datei
  outFile = open("clean." + filename, "w")
  outFile.write(result)
  outFile.close()
  
  return filename, saldoAnfang, saldoEnde


print("Python File cleanUmsatzfile geladen")
