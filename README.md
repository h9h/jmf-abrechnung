# Verein der Musikfreunde Heidelberg

Erstellen der Vereinsabrechnung

## Vorbereitende Arbeiten

Öffne Projekt "RStudio-EÜR.Rproj"

### (1) Lege ein Arbeitsverzeichnis an und setze in den folgenden Dateien basedir auf dieses Verzeichnis:

- [R-Notebook](./RStudio-EÜR/EÜR-Rechnung.Rmd)
- [LaTeX-Ausgabe](./RStudio-EÜR/einnahme-ueberschuss.Rnw)

### (2) Bereite die Dateien vor:

- Kopiere alle relevanten Excel-Dateien in das Arbeitsverzeichnis
- Die CSV-Umsatzdateien der VR Bank müssen folgendermaßen bearbeitet werden:
  - in Excel öffnen und die oberen Zeilen löschen
  - Spaltenkopf "SH" über dem Soll-/Haben-Kennzeichen ergänzen
  - Spalte Kategorie ergänzen und jede Zeile mit der relevanten Kategorie versehen
  - Spalte Semester ergänzen und jede Zeile mit "VJ", "WS", "SS" oder "NJ" versehen (Vor GJ, Winter-, Sommer-Semester und Nach GJ)
  - als xlsx speichern
  
Hierfür gibt es auf eine Methode: prepare.umsatzfiles(), die aus dem Projekt "RStudio-EÜR.Rproj" aufgerufen werden kann.
Dazu diese Datei sourcen und n der Console prepare.umsatzfiles() aufrufen. Die Methode fragt nach dem Verzeichnis.

Falls das nicht klappt, können die Verwendungszweck-Text in Visual Studio Code Editor mittels Search/Replace richtig
konkateniert werden: Suche nach '([^"])\n' ersetze durch '$1 '. 

- Die Barkasse-Dateien aufbereiten
  - mit mindestens den Spalten aus [<code>requiredColumns</code>](./RStudio-EÜR/R/konstanten.R#L1).
- Lege die Datei Initialwerte.R im Arbeitsverzeichnis an, mit Zuweisungen für 
  - jahr
  - konto.vj
  - konto.gj
  - depot.vj
  - depot.gj

### (3) R-Notebook

Es empfiehlt sich, zunächst mit dem R-Notebook zu arbeiten, bis die Abrechnung steht, do dort leicht Zwischenwerte angezeigt werden können.

### (4) PDF erzeugen

Wenn alle Abweichungen bereinigt sind, kann mithilfe von [LaTeX-Ausgabe](./RStudio-EÜR/einnahme-ueberschuss.Rnw) das PDF erzeugt werden.

## Hinweise zur Aufbereitung der Daten

[Hier](./RStudio-EÜR/Template.xlsx) steht ein Excel-Template für die Eingabedateien zur Verfügung.
Dieses enthält die benötigten Spalten und entsprechende Wertelisten für die Einnahme-/Ausgabe-kategorien.

Die Template orientiert sich am Download-Format der VR-Bank Kontoauszüge.

Die Kassenabrechnungen müssen einen Dateinamen beginnend mit "Buch" haben, dann werden dort Prüfungen durchgeführt.

**Beachte**: Nur ".xlsx"-Dateien werden verarbeitet, nicht ".xls"
